<!DOCTYPE html>
<html>
<head>
    <title>Pendaftaran Training | IT man's Maintenance WORKSHOP</title>
</head>

<body>
    <header>
        <h3>Formulir Pendaftaran Training</h3>
    </header>

    <form action="script-input_formulir.php" method="POST">

        <fieldset>

            <p>
                <label for="nama">Nama Lengkap : </label>
                <input type="text" name="nama" placeholder="nama lengkap">
            </p>
            <p>
                <label for="email">Email :</label>
                <input type="text" name="email" placeholder="email">
            </p>
            <p>
                <label for="no_telp">Masukkan nomor telepon(MAX 13 karakter!) : </label>
                <input type="text" name="no_telp" placeholder="nomor telepon">
            </p>
            <p>
                <label for="alamat">Alamat: </label>
                <textarea name="alamat"></textarea>
            </p>
            <p>
                <label for="jenis_kelamin">Jenis Kelamin : </label>
                <select name="jenis_kelamin">
                    <option>Laki-laki</option>
                    <option>Perempuan</option>
                </select>
            </p>
            <p>
                <label for="tempat_lahir">Tempat Lahir : </label>
                <input type="text" name="tempat_lahir" placeholder="tempat lahir">
            </p>
            <p>
                <label for="tanggal_lahir">Tanggal Lahir :</label>
                <input type="date" name="tanggal_lahir" placeholder="tanggal lahir">
            </p>
            <p>
                <label for="jenis_instansi">Tipe Instansi : </label>
                <select name="jenis_instansi">
                    <option>Swasta</option>
                    <option>Negri</option>
                </select>
            </p>
              <p>
                <label for="nama_instansi">Nama Instansi : </label>
                <input type="text" name="nama_instansi" placeholder="nama instansi">
            </p>
            <p>
                <label for="tanggal_training">Tanggal Training : </label>
                <input type="date" name="tanggal_training" placeholder="tanggal Training">
            </p>
            <p>
                <label for="pembayaran">Metode Pembayaran : </label>
                <select name="pembayaran">
                    <option>Cash</option>
                    <option>Credit</option>
                </select>
            </p>
            <p>
                <input type="submit" value="Daftar" name="siap-input">
            </p>

        </fieldset>
        

    </form>
  

    </body>
</html>