-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 28, 2019 at 03:39 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pendaftaran_training`
--

-- --------------------------------------------------------

--
-- Table structure for table `parapendaftar`
--

CREATE TABLE `parapendaftar` (
  `id` int(11) NOT NULL,
  `nama` varchar(64) NOT NULL,
  `email` varchar(64) NOT NULL,
  `no_telp` varchar(16) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `jenis_kelamin` varchar(16) NOT NULL,
  `tempat_lahir` varchar(64) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `jenis_instansi` varchar(16) NOT NULL,
  `nama_instansi` varchar(64) NOT NULL,
  `tanggal_training` date NOT NULL,
  `pembayaran` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `parapendaftar`
--

INSERT INTO `parapendaftar` (`id`, `nama`, `email`, `no_telp`, `alamat`, `jenis_kelamin`, `tempat_lahir`, `tanggal_lahir`, `jenis_instansi`, `nama_instansi`, `tanggal_training`, `pembayaran`) VALUES
(4, 'Marvelous Lee ', 'marvly@lesley.com', '087764532419', 'Chameleon St', 'Laki-laki', 'Bumi', '1998-12-31', 'Swasta', 'Leslie', '2023-01-17', 'Cash');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `parapendaftar`
--
ALTER TABLE `parapendaftar`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `parapendaftar`
--
ALTER TABLE `parapendaftar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
